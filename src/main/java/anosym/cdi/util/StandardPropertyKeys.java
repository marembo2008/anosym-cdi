package anosym.cdi.util;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 9:47:12 PM
 */
public final class StandardPropertyKeys {

  public static final String ACTIVE_PROFILES_KEY = "active.profiles";

}
