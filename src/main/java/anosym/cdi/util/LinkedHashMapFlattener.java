package anosym.cdi.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Nullable;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 18, 2018, 12:22:45 AM
 */
public class LinkedHashMapFlattener {

  @NonNull
  public Map<String, String> flatten(@Nullable final String keyPrefix,
                                     @NonNull final LinkedHashMap<String, Object> props) {
    final Map<String, String> flattenedValues = new HashMap<>();
    props.forEach((key, value) -> {
      if (value instanceof LinkedHashMap) {
        final String nextPrefix = keyPrefix != null ? keyPrefix + "." + key : key;
        final LinkedHashMap<String, Object> nextProps = (LinkedHashMap<String, Object>) value;
        flattenedValues.putAll(flatten(nextPrefix, nextProps));
      } else {
        final String propKey = keyPrefix != null ? keyPrefix + "." + key : key;
        flattenedValues.put(propKey, String.valueOf(value));
      }
    });

    return flattenedValues;
  }

}
