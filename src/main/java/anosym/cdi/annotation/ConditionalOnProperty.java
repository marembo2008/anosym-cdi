package anosym.cdi.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is evaluated long before config-service is bootstrap. The configuration properties must be define as a normal
 * java application properties.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 16, 2018, 11:32:41 PM
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConditionalOnProperty {

    String name();

    String havingValue();

    boolean matchIfMissing() default false;

}
