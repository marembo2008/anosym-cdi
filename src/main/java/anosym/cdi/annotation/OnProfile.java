package anosym.cdi.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 9:22:39 PM
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnProfile {

    /**
     * The set of profile, for which the bean is active.
     */
    String[] value();

}
