package anosym.cdi.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:02:54 PM
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConditionalOnClass {

    /**
     * The fully qualified name of the java class to test for existence through {@link Class#forName(java.lang.String) }
     *
     * @return
     */
    String value();

}
