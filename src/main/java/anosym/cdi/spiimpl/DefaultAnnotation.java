package anosym.cdi.spiimpl;

import jakarta.enterprise.inject.Default;
import jakarta.enterprise.util.AnnotationLiteral;

@SuppressWarnings(value = "AnnotationAsSuperInterface")
public final class DefaultAnnotation extends AnnotationLiteral<Default> implements Default {

  private static final long serialVersionUID = -1485969418517333998L;

}
