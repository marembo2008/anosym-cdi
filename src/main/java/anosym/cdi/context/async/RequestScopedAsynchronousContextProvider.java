package anosym.cdi.context.async;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.jboss.weld.context.bound.BoundRequestContext;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextConsumer;
import anosym.async.context.AsynchronousContextRemover;
import anosym.async.context.AsynchronousContextSupplier;
import anosym.async.context.ForAsynchronousContext;
import anosym.async.typesafe.TypeSafeKey;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import static anosym.async.typesafe.TypeSafeKeys.key;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 11:20:36 AM
 */
@Slf4j
@ApplicationScoped
@ForAsynchronousContext(RequestScopedDataStore.class)
class RequestScopedAsynchronousContextProvider implements AsynchronousContextSupplier<RequestScopedDataStore>,
        AsynchronousContextConsumer<RequestScopedDataStore>,
        AsynchronousContextRemover<RequestScopedDataStore> {

    private static final TypeSafeKey<RequestScopedDataStore> REQUEST_SCOPED_CONTEXT
            = key("propagated-context.requestScoped", RequestScopedDataStore.class);

    @Inject
    private BoundRequestContext boundRequestContext;

    @Override
    public Optional<AsynchronousContext<RequestScopedDataStore>> provide() {
        //We simply initialize the data store
        return AsynchronousContext
                .<RequestScopedDataStore>builder()
                .context(new RequestScopedDataStore())
                .contextKey(REQUEST_SCOPED_CONTEXT)
                .build()
                .toOptional();
    }

    @Override
    public void consume(@Nonnull
            final AsynchronousContext<RequestScopedDataStore> asynchronousContext) {
        checkNotNull(asynchronousContext, "The asynchronousContext must not be null");

        // We activate the bounded request context
        boundRequestContext.associate(asynchronousContext.getContext().getDataStore());
        boundRequestContext.activate();
    }

    @Override
    public void remove(@Nonnull
            final AsynchronousContext<RequestScopedDataStore> asynchronousContext) {
        checkNotNull(asynchronousContext, "The asynchronousContext must not be null");

        final var dataStore = asynchronousContext.getContext().getDataStore();
        try {
            /* Invalidate the request (all bean instances will be scheduled for destruction) */
            boundRequestContext.invalidate();

            /* Deactivate the request, causing all bean instances to be destroyed (as the context is invalid) */
            boundRequestContext.deactivate();
        } finally {
            /* Ensure that whatever happens we dissociate to prevent any memory leaks */
            boundRequestContext.dissociate(dataStore);
        }
    }

}
