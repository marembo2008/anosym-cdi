package anosym.cdi.context.async;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import lombok.Data;

/**
 *
 * @author marembo
 */
@Data
final class RequestScopedDataStore {

    @Nonnull
    private final Map<String, Object> dataStore = new HashMap<>();

}
