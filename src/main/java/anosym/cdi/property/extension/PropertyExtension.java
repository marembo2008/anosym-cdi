package anosym.cdi.property.extension;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import anosym.cdi.property.handler.InitializationHandler;
import anosym.cdi.util.LinkedHashMapFlattener;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.Extension;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import org.yaml.snakeyaml.Yaml;

import static anosym.cdi.util.StandardPropertyKeys.ACTIVE_PROFILES_KEY;
import static java.lang.String.format;
import static java.lang.System.getProperties;
import static java.lang.System.getenv;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.atteo.classindex.ClassIndex.getSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 16, 2018, 11:37:01 PM
 */
@Slf4j
public class PropertyExtension implements Extension {

    private static final Splitter COMMA_SPLITTER = Splitter.on(",");

    private static final String APPLICATION_FILE_PROP_TEMPLATE = "/application%s.properties";

    private static final String APPLICATION_FILE_YML_TEMPLATE = "/application%s.yml";

    private static final String APPLICATION_FILE_YAML_TEMPLATE = "/application%s.yaml";

    private static final Yaml YAML = new Yaml();

    private static final LinkedHashMapFlattener LINKED_HASH_MAP_FLATTENER = new LinkedHashMapFlattener();

    private static final List<InitializationHandler> INITIALIZATION_HANDLERS;

    public static final Properties PROPERTIES = new Properties();

    static {
        //initial priority properties
        loadPriorityProperties();

        //override with application defined properties for specific profiles.
        final String activeProfiles = PROPERTIES.getProperty(ACTIVE_PROFILES_KEY, "");

        log.info("Active Profiles: {}", activeProfiles);

        COMMA_SPLITTER.splitToList(activeProfiles)
                .stream()
                .map((profile) -> profile.isEmpty() ? "" : "-" + profile)
                .flatMap(PropertyExtension::profilePropPaths)
                .peek((profilePath) -> log.info("Loading properties from file: {}", profilePath))
                .sorted()
                .map(PropertyExtension::loadProperties)
                .filter(Objects::nonNull)
                .forEach(PROPERTIES::putAll);

        //override with priority properties
        loadPriorityProperties();

        //init handlers
        final var classLoader = PropertyExtension.class.getClassLoader();
        INITIALIZATION_HANDLERS = ImmutableList
                .copyOf(getSubclasses(InitializationHandler.class, classLoader))
                .stream()
                .map(PropertyExtension::init)
                .collect(toList());
    }

    private static void loadPriorityProperties() {
        //initlaize properties with system properties
        getProperties().forEach(PROPERTIES::putIfAbsent);

        //override properties with environment.
        getenv().forEach(PROPERTIES::putIfAbsent);
    }

    @NonNull
    private static Stream<String> profilePropPaths(@NonNull final String profile) {
        return ImmutableList
                .of(format(APPLICATION_FILE_PROP_TEMPLATE, profile),
                        format(APPLICATION_FILE_YAML_TEMPLATE, profile),
                        format(APPLICATION_FILE_YML_TEMPLATE, profile))
                .stream();
    }

    @NonNull
    private static Map<String, String> loadProperties(@NonNull final String profilePropPath) {
        final InputStream propInn = PropertyExtension.class.getResourceAsStream(profilePropPath);
        if (propInn == null) {
            return ImmutableMap.of();
        }

        if (profilePropPath.endsWith(".yml") || profilePropPath.endsWith(".yaml")) {
            final Object props = YAML.load(propInn);
            return LINKED_HASH_MAP_FLATTENER.flatten(null, (LinkedHashMap<String, Object>) props);
        }

        try {
            final Properties props = new Properties();
            props.load(propInn);
            return props
                    .entrySet()
                    .stream()
                    .collect(toMap((e) -> String.valueOf(e.getKey()), (e) -> String.valueOf(e.getValue())));
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @NonNull
    private static InitializationHandler init(@NonNull final Class<? extends InitializationHandler> handlerClass) {
        try {
            return handlerClass.newInstance();
        } catch (final InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void onProcessAnnotated(@Observes final ProcessAnnotatedType<?> processAnnotatedType) {
        INITIALIZATION_HANDLERS.forEach((handler) -> handler.handle(PROPERTIES, processAnnotatedType));
    }

}
