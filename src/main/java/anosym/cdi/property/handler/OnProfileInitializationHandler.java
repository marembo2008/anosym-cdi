package anosym.cdi.property.handler;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.google.common.base.Splitter;

import anosym.cdi.annotation.OnProfile;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static anosym.cdi.util.StandardPropertyKeys.ACTIVE_PROFILES_KEY;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 10:07:00 PM
 */
@Slf4j
public class OnProfileInitializationHandler implements InitializationHandler {

  private static final Splitter COMMA_SPLITTER = Splitter.on(",");

  @Override
  public void handle(@NonNull final Properties properties, @NonNull final ProcessAnnotatedType<?> processAnnotatedType) {
    log.info("Processing OnProfile on <{}>", processAnnotatedType.getAnnotatedType().getJavaClass());

    final OnProfile onProfile = processAnnotatedType.getAnnotatedType().getAnnotation(OnProfile.class);
    if (onProfile == null) {
      return;
    }

    final String activeProfiles = properties.getProperty(ACTIVE_PROFILES_KEY, "");
    final List<String> profiles = COMMA_SPLITTER.splitToList(activeProfiles)
            .stream()
            .filter((profile) -> !profile.isEmpty())
            .collect(toList());
    Arrays.stream(onProfile.value())
            .filter((profile) -> isDisabled(profile, profiles))
            .findFirst()
            .ifPresent((profile) -> {
              log.info("Vetoing <{}> for profile <{}>",
                       processAnnotatedType.getAnnotatedType().getJavaClass(),
                       profile);
              processAnnotatedType.veto();
            });
  }

  private boolean isDisabled(@NonNull final String profile, @NonNull final List<String> profiles) {
    return profile.startsWith("!") ? (profiles.contains(profile.substring(1))) : !profiles.contains(profile);
  }

}
