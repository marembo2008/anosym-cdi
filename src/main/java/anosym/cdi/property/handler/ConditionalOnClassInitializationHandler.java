package anosym.cdi.property.handler;

import java.util.Properties;

import anosym.cdi.annotation.ConditionalOnClass;
import jakarta.enterprise.inject.spi.AnnotatedType;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:04:29 PM
 */
@Slf4j
public class ConditionalOnClassInitializationHandler implements InitializationHandler {

  @Override
  public void handle(@NonNull final Properties properties, @NonNull final ProcessAnnotatedType<?> processAnnotatedType) {
    final AnnotatedType<?> annotatedType = processAnnotatedType.getAnnotatedType();
    final ConditionalOnClass conditionalOnClass = annotatedType.getAnnotation(ConditionalOnClass.class);
    if (conditionalOnClass == null) {
      return;
    }

    try {
      Class.forName(conditionalOnClass.value());

      //class exists, do not veto the bean
      log.info("Class <{}> found, for annotated type <{}>", conditionalOnClass.value(), annotatedType
               .getJavaClass());
    } catch (final ClassNotFoundException ex) {
      //Class does not exists, veto the class
      log.info("No class defined for <{}>. vetoeing bean <{}>", conditionalOnClass.value(), annotatedType
               .getJavaClass());

      processAnnotatedType.veto();
    }
  }

}
