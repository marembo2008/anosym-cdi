package anosym.cdi.property.handler;

import java.util.Properties;

import anosym.cdi.annotation.ConditionalOnMissingClass;
import jakarta.enterprise.inject.spi.AnnotatedType;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:04:29 PM
 */
@Slf4j
public class ConditionalOnMissingClassInitializationHandler implements InitializationHandler {

  @Override
  public void handle(@NonNull final Properties properties, @NonNull final ProcessAnnotatedType<?> processAnnotatedType) {
    final AnnotatedType<?> annotatedType = processAnnotatedType.getAnnotatedType();
    final ConditionalOnMissingClass conditionalOnMissingClass
            = annotatedType.getAnnotation(ConditionalOnMissingClass.class);
    if (conditionalOnMissingClass == null) {
      return;
    }

    try {
      Class.forName(conditionalOnMissingClass.value());

      //class exists, veto the bean
      log.info("Class <{}> found, vetoing the <{}>", conditionalOnMissingClass.value(), annotatedType
               .getJavaClass());
      processAnnotatedType.veto();
    } catch (final ClassNotFoundException ex) {
      //Class does not exists, hence bean allowed, ignore exception
      log.info("No class defined for <{}>. Accepting bean <{}>", conditionalOnMissingClass.value(), annotatedType
               .getJavaClass());
    }
  }

}
