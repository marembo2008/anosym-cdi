package anosym.cdi.property.handler;

import java.util.Properties;

import anosym.cdi.annotation.ConditionalOnProperty;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 10:07:00 PM
 */
@Slf4j
public class ConditionalOnPropertyInitializationHandler implements InitializationHandler {

  @Override
  public void handle(@NonNull final Properties properties, @NonNull final ProcessAnnotatedType<?> processAnnotatedType) {
    final ConditionalOnProperty conditionalOnProperty = processAnnotatedType
            .getAnnotatedType()
            .getAnnotation(ConditionalOnProperty.class);
    if (conditionalOnProperty == null) {
      return;
    }

    final String property = properties.getProperty(conditionalOnProperty.name());
    if (property == null && !conditionalOnProperty.matchIfMissing()) {
      log.info("Vetoing <{}> due to missing property <{}>",
               processAnnotatedType.getAnnotatedType().getJavaClass(),
               conditionalOnProperty.name());
      processAnnotatedType.veto();
      return;
    }

    if (property != null && !conditionalOnProperty.havingValue().equals(property)) {
      log.info("Vetoing <{}> due to unmatched property <{}>. Expected <{}>, but found <{}>",
               processAnnotatedType.getAnnotatedType().getJavaClass(),
               conditionalOnProperty.name(),
               conditionalOnProperty.havingValue(),
               property);
      processAnnotatedType.veto();

    }
  }

}
