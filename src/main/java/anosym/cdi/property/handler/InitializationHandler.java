package anosym.cdi.property.handler;

import java.util.Properties;

import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import lombok.NonNull;
import org.atteo.classindex.IndexSubclasses;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 9:48:55 PM
 */
@IndexSubclasses
public interface InitializationHandler {

  void handle(@NonNull final Properties properties, @NonNull final ProcessAnnotatedType<?> processAnnotatedType);

}
