package anosym.cdi.property.producer;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import com.google.common.collect.ImmutableList;

import anosym.cdi.annotation.OnProperty;
import anosym.cdi.property.extension.PropertyExtension;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 11:43:34 PM
 */
@ApplicationScoped
public class PropertyProvider {

  private final Properties properties = PropertyExtension.PROPERTIES;

  @Produces
  @OnProperty("")
  public String produceString(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).orElse(null);
  }

  @Produces
  @OnProperty("")
  public Integer produceInteger(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map(Integer::parseInt).orElse(null);
  }

  @Produces
  @OnProperty("")
  public Long produceLong(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map(Long::parseLong).orElse(null);
  }

  @Produces
  @OnProperty("")
  public Double produceDouble(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map(Double::parseDouble).orElse(null);
  }

  @Produces
  @OnProperty("")
  public Float produceFloat(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map(Float::parseFloat).orElse(null);
  }

  @Produces
  @OnProperty("")
  public BigDecimal produceBigDecimal(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map(BigDecimal::new).orElse(null);
  }

  @Produces
  @OnProperty("")
  public List<String> productListOfString(@NonNull final InjectionPoint ip) {
    return getStringValue(ip).map((s) -> s.split(",")).map(ImmutableList::copyOf).orElse(ImmutableList.of());
  }

  @NonNull
  private Optional<String> getStringValue(@NonNull final InjectionPoint ip) {
    final OnProperty onProperty = ip.getAnnotated().getAnnotation(OnProperty.class);
    return Optional.ofNullable(properties.getProperty(onProperty.value()));
  }

}
