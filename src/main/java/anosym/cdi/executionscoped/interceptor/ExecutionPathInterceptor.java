package anosym.cdi.executionscoped.interceptor;

import static anosym.cdi.executionscoped.context.ExecutionIdHolder.getExecutionId;

import java.io.Serializable;
import java.util.UUID;

import anosym.cdi.executionscoped.ExecutionPath;
import anosym.cdi.executionscoped.context.ExecutionScopedContext;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.Dependent;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:44:34 PM
 */
@Slf4j
@Dependent
@Interceptor
@ExecutionPath
@Priority(Interceptor.Priority.LIBRARY_BEFORE + 100)
public class ExecutionPathInterceptor implements Serializable {

  private static final ExecutionScopedContext CONTEXT = ExecutionScopedContext.getInstance();

  @AroundInvoke
  public Object intercept(@NonNull final InvocationContext invocationContext) throws Exception {
    if (CONTEXT.isActive()) {
      log.debug("Execution path is in an active context. Current execution context: {}", getExecutionId());
      return invocationContext.proceed();
    }

    final UUID executionId = UUID.randomUUID();

    log.debug("Starting execution scope for id: {}", executionId);

    CONTEXT.startExecutionContext(executionId);

    try {
      return invocationContext.proceed();
    } finally {

      log.debug("Completing execution scope for id: {}", executionId);

      CONTEXT.completeExecutionContext(executionId);
    }
  }

}
