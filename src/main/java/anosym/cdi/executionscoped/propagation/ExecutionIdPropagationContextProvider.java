package anosym.cdi.executionscoped.propagation;

import static anosym.async.typesafe.TypeSafeKeys.key;

import java.util.Optional;
import java.util.UUID;

import anosym.async.context.AsynchronousContext;
import anosym.async.context.AsynchronousContextConsumer;
import anosym.async.context.AsynchronousContextRemover;
import anosym.async.context.AsynchronousContextSupplier;
import anosym.async.context.ForAsynchronousContext;
import anosym.async.typesafe.TypeSafeKey;
import anosym.cdi.executionscoped.context.ExecutionIdHolder;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 10:43:25 PM
 */
@Slf4j
@ApplicationScoped
@ForAsynchronousContext(ExecutionId.class)
public class ExecutionIdPropagationContextProvider implements AsynchronousContextSupplier<ExecutionId>,
                                                              AsynchronousContextConsumer<ExecutionId>,
                                                              AsynchronousContextRemover<ExecutionId> {

  private static final TypeSafeKey<ExecutionId> EXECUTION_ID_KEY
          = key("propagated-context.executionId", ExecutionId.class);

  @Override
  public Optional<AsynchronousContext<ExecutionId>> provide() {
    final UUID executionId = ExecutionIdHolder.getExecutionId();
    if (executionId == null) {
      return Optional.empty();
    }

    log.debug("Propagating execution id<{}>", executionId);

    final AsynchronousContext<ExecutionId> executionIdContext = AsynchronousContext
            .<ExecutionId>builder()
            .context(new ExecutionId(executionId))
            .contextKey(EXECUTION_ID_KEY)
            .build();
    return Optional.of(executionIdContext);
  }

  @Override
  public void consume(@NonNull final AsynchronousContext<ExecutionId> asynchronousContext) {
    final ExecutionId executionId = asynchronousContext.getContext();
    ExecutionIdHolder.setExecutionId(executionId.getExecutionId());
  }

  @Override
  public void remove(@NonNull final AsynchronousContext<ExecutionId> asynchronousContext) {
    ExecutionIdHolder.unsetExecutionId();
  }

}
