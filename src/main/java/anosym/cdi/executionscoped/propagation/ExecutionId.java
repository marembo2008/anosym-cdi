package anosym.cdi.executionscoped.propagation;

import java.util.UUID;

import javax.annotation.Nonnull;

import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 10:42:24 PM
 */
@Data
public class ExecutionId {

    @Nonnull
    private final UUID executionId;

}
