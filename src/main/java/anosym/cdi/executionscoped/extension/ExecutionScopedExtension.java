package anosym.cdi.executionscoped.extension;

import anosym.cdi.executionscoped.context.ExecutionScopedContext;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.AfterBeanDiscovery;
import jakarta.enterprise.inject.spi.Extension;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:34:14 PM
 */
@Slf4j
public class ExecutionScopedExtension implements Extension {

  public void registerContext(@Observes final AfterBeanDiscovery afterBeanDiscovery) {
    log.info("Registering @ExecutionScoped context");

    afterBeanDiscovery.addContext(ExecutionScopedContext.getInstance());
  }

}
