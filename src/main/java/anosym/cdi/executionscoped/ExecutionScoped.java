package anosym.cdi.executionscoped;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.context.NormalScope;

/**
 * An execution scoped bean is destroyed at the end of a well demarcated execution path.
 *
 * The entry of an execution is marked by annotating a method or class with {@link ExecutionPath} which indicates that
 * the method (or every method of the bean) will be using an ExecutionScoped bean of any of its injected beans which is
 * in Execution Scope.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:38:02 PM
 */
@NormalScope
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface ExecutionScoped {
}
