package anosym.cdi.executionscoped;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.enterprise.util.Nonbinding;
import jakarta.interceptor.InterceptorBinding;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:41:06 PM
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ExecutionPath {

  @Nonbinding
  PropagationType propagation() default PropagationType.JOIN;

  enum PropagationType {

    JOIN,
    NEW

  }

}
