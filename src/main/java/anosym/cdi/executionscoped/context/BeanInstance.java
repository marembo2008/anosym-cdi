package anosym.cdi.executionscoped.context;

import jakarta.enterprise.context.spi.Contextual;
import jakarta.enterprise.context.spi.CreationalContext;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 10:31:46 PM
 */
@Data
final class BeanInstance<T> {

  @NonNull
  private final T object;

  @NonNull
  private final Contextual<T> contextual;

  @NonNull
  private final CreationalContext<T> context;

}
