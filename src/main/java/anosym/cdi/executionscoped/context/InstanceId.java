package anosym.cdi.executionscoped.context;

import java.util.UUID;

import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 10:31:34 PM
 */
@Data
final class InstanceId {

    @NonNull
    private final UUID executionId;

    @NonNull
    private final Class<?> beanClass;

}
