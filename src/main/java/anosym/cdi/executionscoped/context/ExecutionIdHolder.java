package anosym.cdi.executionscoped.context;

import java.util.UUID;

import javax.annotation.Nullable;

import lombok.NonNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 10:54:23 PM
 */
public final class ExecutionIdHolder {

    private static final ThreadLocal<UUID> EXECUTION_ID = new ThreadLocal<>();

    @Nullable
    public static UUID getExecutionId() {
        return EXECUTION_ID.get();
    }

    public static void setExecutionId(@NonNull final UUID executionId) {
        EXECUTION_ID.set(executionId);
    }

    public static void unsetExecutionId() {
        EXECUTION_ID.remove();
    }

}
