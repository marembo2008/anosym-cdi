package anosym.cdi.executionscoped.context;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import anosym.cdi.executionscoped.ExecutionScoped;
import jakarta.enterprise.context.ContextNotActiveException;
import jakarta.enterprise.context.spi.AlterableContext;
import jakarta.enterprise.context.spi.Contextual;
import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.spi.Bean;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 7:35:22 PM
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExecutionScopedContext implements AlterableContext {

  private final Map<InstanceId, BeanInstance> beans = new ConcurrentHashMap<>();

  @NonNull
  public static ExecutionScopedContext getInstance() {
    return ExecutionScopedContextHolder.INSTANCE.executionScopedContext;
  }

  @Override
  public void destroy(@NonNull final Contextual<?> contextual) {
    final Class<?> beanClass = ((Bean<?>) contextual).getBeanClass();
    destroyInstances((instanceId) -> instanceId.getBeanClass().equals(beanClass));
  }

  @NonNull
  @Override
  public Class<? extends Annotation> getScope() {
    return ExecutionScoped.class;
  }

  @Override
  public <T> T get(@NonNull final Contextual<T> contextual, @Nullable final CreationalContext<T> creationalContext) {
    final Class<?> beanClass = ((Bean<T>) contextual).getBeanClass();

    log.debug("Resolving bean <{}> in @ExecutionScoped context", beanClass);

    checkActive(beanClass);

    final UUID executionId = ExecutionIdHolder.getExecutionId();
    final InstanceId instanceId = new InstanceId(executionId, beanClass);
    if (creationalContext == null) {
      return (T) Optional.ofNullable(beans.get(instanceId))
              .map(BeanInstance::getObject)
              .orElse(null);
    }

    final T objInstance = contextual.create(creationalContext);
    checkState(objInstance != null, "No instance created for bean class <%s>", beanClass);

    final BeanInstance<T> beanInstance = new BeanInstance<>(objInstance, contextual, creationalContext);
    beans.put(instanceId, beanInstance);

    return objInstance;
  }

  @Override
  public <T> T get(@NonNull final Contextual<T> contextual) {
    return get(contextual, null);
  }

  @Override
  public boolean isActive() {
    return ExecutionIdHolder.getExecutionId() != null;
  }

  public void startExecutionContext(@NonNull final UUID executionId) {
    ExecutionIdHolder.setExecutionId(executionId);
  }

  public void completeExecutionContext(@NonNull final UUID executionId) {
    ExecutionIdHolder.unsetExecutionId();
    destroyInstances((instanceId) -> instanceId.getExecutionId().equals(executionId));
  }

  private void destroyInstances(@NonNull final Predicate<InstanceId> instanceIdSelector) {
    //destroy the bean
    final List<InstanceId> outOfScopeInstanceIds = beans.keySet()
            .stream()
            .filter(instanceIdSelector)
            .collect(toList());

    log.debug("Destroying the following instance ids which have gotten out of scope: {}", outOfScopeInstanceIds);

    outOfScopeInstanceIds.forEach((instanceId) -> {
      final BeanInstance instance = beans.get(instanceId);
      instance.getContextual().destroy(instance.getObject(), instance.getContext());

      //remove from bean list
      beans.remove(instanceId);
    });
  }

  private void checkActive(final Class<?> beanClass) {
    if (isActive()) {
      return;
    }

    throw new ContextNotActiveException(
            format("Execution context not started. Did you annotate bean <%s> with @ExecutiionPath?", beanClass));
  }

  private enum ExecutionScopedContextHolder {

    INSTANCE;

    private final ExecutionScopedContext executionScopedContext = new ExecutionScopedContext();

  }

}
