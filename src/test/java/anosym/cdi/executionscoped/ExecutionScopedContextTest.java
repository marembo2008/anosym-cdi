package anosym.cdi.executionscoped;

import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static java.util.concurrent.CompletableFuture.supplyAsync;

import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import anosym.async.executor.ContextManagedExecutor;
import anosym.tests.junit.CdiJUnitExtension;
import jakarta.enterprise.context.ContextNotActiveException;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 10:38:25 PM
 */
@ExtendWith(CdiJUnitExtension.class)
public class ExecutionScopedContextTest {

  @Inject
  private ExecutionService executionService;

  @Inject
  private ContextManagedExecutor executor;

  @Test
  public void assertExecutionActive() {
    executionService.execute();
  }

  @Test
  public void assertExecutionActiveInAsynchronousContext() {
    final int sum = IntStream.range(0, 4)
            .mapToObj((ix) -> supplyAsync(() -> executionService.multipleIndex(ix), executor))
            .collect(toList())
            .stream()
            .map((future) -> {
              try {
                return future.get();
              } catch (InterruptedException | ExecutionException ex) {
                throw new RuntimeException(ex);
              }
            })
            .collect(summingInt(Integer::intValue));
    assertThat(sum, equalTo(14));
  }

  @Test
  public void assertExecutionNotActive() {
    assertThrows(ContextNotActiveException.class, () -> executionService.executeWithNoContext());
  }

}
