package anosym.cdi.executionscoped;

import anosym.async.logging.TraceId;
import anosym.async.logging.TraceIdProvider;
import anosym.cdi.annotation.ConditionalOnMissingClass;
import jakarta.enterprise.context.ApplicationScoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:43:17 PM
 */
@ApplicationScoped
@ConditionalOnMissingClass("jakarta.servlet.ServletRequestListener")
public class TestTraceIdProvider implements TraceIdProvider {

  private static final ThreadLocal<TraceId> TRACE_ID = ThreadLocal.withInitial(TraceId::createTraceId);

  @Override
  public TraceId getTraceId() {
    return TRACE_ID.get();
  }

  @Override
  public void setTraceId(TraceId traceId) {
    TRACE_ID.set(traceId);
  }

  @Override
  public void removeTraceId(TraceId traceId) {
    TRACE_ID.remove();
  }

}
