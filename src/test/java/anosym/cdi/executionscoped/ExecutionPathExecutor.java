package anosym.cdi.executionscoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 11, 2018, 10:37:24 PM
 */
@ExecutionScoped
public class ExecutionPathExecutor {

    public void execute() {
    }

    public int multipleIndex(final int index) {
        return index * index;
    }

}
