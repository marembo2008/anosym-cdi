package anosym.cdi.executionscoped;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 28, 2021, 11:47:43 PM
 */
@ApplicationScoped
public class ExecutionService {

  @Inject
  private ExecutionPathExecutor pathExecutor;

  @ExecutionPath
  public void execute() {
    pathExecutor.execute();
  }

  public void executeWithNoContext() {
    pathExecutor.execute();
  }

  @ExecutionPath
  public int multipleIndex(final int index) {
    return pathExecutor.multipleIndex(index);
  }

}
