package anosym.cdi.executionscoped;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import anosym.cdi.annotation.ConditionalOnMissingClass;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Disposes;
import jakarta.enterprise.inject.Produces;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 16, 2018, 11:14:37 PM
 */
@ApplicationScoped
@ConditionalOnMissingClass("jakarta.enterprise.concurrent.ManagedExecutorService")
public class TestExecutionServiceProvider {

  @Produces
  @Dependent
  public ExecutorService executorService() {
    return Executors.newFixedThreadPool(10);
  }

  public void shutdown(@Disposes final ExecutorService executorService) {
    executorService.shutdown();
  }

}
