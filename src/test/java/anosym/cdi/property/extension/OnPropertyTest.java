package anosym.cdi.property.extension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import anosym.cdi.annotation.OnProperty;
import anosym.tests.cdi.ActiveProfile;
import anosym.tests.junit.CdiJUnitExtension;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 10:32:45 PM
 */
@Slf4j
@ActiveProfile("test")
@ActiveProfile("profile")
@ExtendWith(CdiJUnitExtension.class)
public class OnPropertyTest {

  @Test
  public void verifyOnProperty() {
    final OnPropertyBean bean = CDI.current().select(OnPropertyBean.class).get();
    assertThat(bean.getStates(), hasItems("control", "alienated"));
  }

  @ApplicationScoped
  public static class OnPropertyBean {

    @Getter
    @Inject
    @OnProperty("cdi.extension.state")
    private List<String> states;

  }

}
