package anosym.cdi.property.extension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.google.common.collect.ImmutableList;

import anosym.cdi.annotation.ConditionalOnProperty;
import anosym.tests.cdi.ActiveProfile;
import anosym.tests.junit.CdiJUnitExtension;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 10:32:45 PM
 */
@Slf4j
@ActiveProfile("test")
@ExtendWith(CdiJUnitExtension.class)
public class ConditionalOnPropertyTest {

  @Test
  public void verifyOnlyBean1Enaled() {
    final List<TestBean> testBeans = ImmutableList.copyOf(CDI.current().select(TestBean.class));
    assertThat(testBeans, hasSize(1));
    assertThat(testBeans, hasItem(instanceOf(TestBean1.class)));
  }

  public static interface TestBean {

    void doTest();

  }

  @ApplicationScoped
  @ConditionalOnProperty(name = "cdi.extension.enabledBean", havingValue = "bean1")
  public static class TestBean1 implements TestBean {

    @Override
    public void doTest() {
      log.info("Tested Bean1");
    }

  }

  @ApplicationScoped
  @ConditionalOnProperty(name = "cdi.extension.enabledBean", havingValue = "bean2")
  public static class TestBean2 implements TestBean {

    @Override
    public void doTest() {
      log.info("Tested Bean2");
    }

  }

}
